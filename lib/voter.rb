require './lib/sitebot/sitebot'
require './lib/sms-reg/service'
require 'log4r'
include Log4r

module Voter extend self

  @logger = Logger.new('vote')
  fo = FileOutputter.new('file', filename: './vote.log')
  co = StdoutOutputter.new 'console'
  fo.level = Log4r::DEBUG
  co.level = Log4r::INFO
  @logger.outputters = [fo, co]

  @sms_service = SMSReg::Service

  def vote
    @bot = Sitebot.create('orenshow')
    begin
      open_session
      use_number(get_number)
      use_code(get_code)
      confirm_vote
      close_session
    rescue Exception => e
      log("Во время выполнения произошла ошибка: #{e.message}", true)
      log("Error: #{e.message}. Backtrace: \n#{e.backtrace.join("\n")}")
      close_session
      raise e
    end
  end

  private

  def log(msg, console = false)
    if console
      @logger.info "#{Time.now.strftime('%H:%M')}: #{msg}"
    else
      @logger.debug "#{Time.now.strftime('%d.%m.%y %H:%M:%S')} - #{msg}"
    end
  end

  def open_session
    log 'Открытие сайта...', true
    @bot.begin_session
    @bot.ensure_vote_ready
    log 'Сайт открыт, выбрано голосование', true
  end

  def get_number
    log 'Получение номера...', true
    number = @sms_service.get_number
    log "Получен номер: #{number}", true
    number
  end

  def use_number(number)
    log 'Вводим номер на сайт', true
    number[0] = ''
    @bot.type_number(number)
    @bot.ensure_number_typed(number)
    log 'Номер введен', true
  end

  def get_code
    log 'Получение кода смс...', true
    code = @sms_service.get_code
    log "Получен код: #{code}", true

  end

  def use_code(code)
    log 'Вводим код на сайт', true
    @bot.type_code(code)
    log 'Код введён', true
  end

  def confirm_vote
    log 'Голос успешно подтверждён!', true
  end

  def close_session
    @bot.close_session
    log 'Закрываем сайт', true
  end
end