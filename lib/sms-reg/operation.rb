require './lib/sms-reg/client'

module SMSReg
class Operation

  attr_reader :tzid, :service, :number, :message

  def self.get_number
    result = SMSReg::Client.get_number
    self.new(result['tzid'])
  end

  def initialize(tzid)
    @tzid = tzid
    @service = nil
    @number = nil
    @message = nil
    @client = SMSReg::Client
    get_state
  end

  # TZ_INPOOL — операция ожидает выделения номера
  # TZ_NUM_PREPARE — выдан номер, ожидается выполнение метода SetReady
  # TZ_NUM_WAIT — ожидается ответ
  # TZ_NUM_ANSWER — поступил ответ
  # TZ_NUM_WAIT2 — ожидается уточнение полученного кода
  # TZ_NUM_ANSWER2 — поступил ответ после уточнения
  # WARNING_NO_NUMS — нету подходящих номеров
  #
  # Также если время по операции уже истекло то получите следующие значения:
  # TZ_OVER_OK — операция завершена
  # TZ_OVER_GR — операция отмечена как ошибочная
  # TZ_OVER_EMPTY — ответ не поступил за отведенное время
  # TZ_OVER_NR — вы не отправили запрос методом setReady
  # TZ_OVER2_EMPTY — уточнение не поступило за отведенное время
  # TZ_OVER2_OK — операция завершена после уточнения
  # TZ_DELETED — операция удалена, средства возвращены
  def get_state
    result = @client.get_state(@tzid)
    @service = result['service']
    @number = result['number']
    @message = result['msg']
    result['response']
  end

  def set_ready
    @client.set_ready(@tzid)['response']
  end

  def set_ok
    @client.set_operation_ok(@tzid)['response']
  end

  def set_over
    @client.set_operation_over(@tzid)['response']
  end

  def set_used
    @client.set_operation_used(@tzid)['response']
  end

  def set_revise
    @client.set_operation_revise(@tzid)['response']
  end
end
end
