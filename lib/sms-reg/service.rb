require './lib/sms-reg/operation'
module SMSReg
  module Service extend self

    WAITING_TIME = 15
    @last_operation = nil

    def get_number
      operation = SMSReg::Operation.get_number
      begin
        sleep WAITING_TIME
        state = operation.get_state
      end while(state == 'TZ_INPOOL')
      raise Exception, "Unable to get number - #{state}" unless state == 'TZ_NUM_PREPARE'
      @last_operation = operation
      @last_operation.number
    end

    def get_code
      @last_operation.set_ready
      begin
        sleep WAITING_TIME
        state = @last_operation.get_state
      end while(state == 'TZ_NUM_WAIT')
      raise Exception, "Unable to get code - #{state}" unless state == 'TZ_NUM_ANSWER'
      @last_operation.message
    end

    def set_ok
      @last_operation.set_ok
    end

  end
end