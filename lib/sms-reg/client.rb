require 'net/http'
require 'uri'
require 'json'

module SMSReg

  class FailedRequest < RuntimeError; end
  class WrongRequest < RuntimeError; end

  module Client
    extend self

    SERVICE_URL_MASK = 'http://api.sms-reg.com/METHOD_NAME.php'
    API_KEY = 'ap3s1d5wra7npu8qr9gf6sbrbvco8piv'

    def get_number(country: 'ru', service: 'other')
      method_query('getNum', country: country, service: service)
    end

    # Получив номер в запросе getNum, Вы используете выделенный телефонный номер в необходимом Вам сервисе,
    # после чего следует сообщить о том что смс отправлено и вы готовы принять ответ из смс.
    def set_ready(tzid)
      method_query('setReady', tzid: tzid)
    end

    # Возвращае состояние выбранной операции.
    # После успешного выполнения получите ответ в формате JSON, в следующем виде:
    # {response: RESPONSE, service:SERVICE, number:NOMER, msg:ANSWER } ,
    # NOMER — выделенный номер, ANSWER — код из смс, SERVICE — сервис, значения такие же как в методе getNum
    #
    # RESPONSE принимает одно из следующих значений:
    # TZ_INPOOL — операция ожидает выделения номера
    # TZ_NUM_PREPARE — выдан номер, ожидается выполнение метода SetReady
    # TZ_NUM_WAIT — ожидается ответ
    # TZ_NUM_ANSWER — поступил ответ
    # TZ_NUM_WAIT2 — ожидается уточнение полученного кода
    # TZ_NUM_ANSWER2 — поступил ответ после уточнения
    # WARNING_NO_NUMS — нету подходящих номеров
    #
    # Также если время по операции уже истекло то получите следующие значения:
    # TZ_OVER_OK — операция завершена
    # TZ_OVER_GR — операция отмечена как ошибочная
    # TZ_OVER_EMPTY — ответ не поступил за отведенное время
    # TZ_OVER_NR — вы не отправили запрос методом setReady
    # TZ_OVER2_EMPTY — уточнение не поступило за отведенное время
    # TZ_OVER2_OK — операция завершена после уточнения
    # TZ_DELETED — операция удалена, средства возвращены

    def get_state(tzid)
      method_query('getState', tzid: tzid)
    end

    # Параметры:
    # opstate Определяет какие операции показать:
    #   active — текущие незавершенные операции;
    #   completed — операции, которые уже завершились;
    #   не указание параметра равно значению active;
    # count - Количество операций, информацию по которым нужно получить.
    #   int (числовое значение), по умолчанию равно 100, максимум 1000.
    #
    # Результат:
    # После успешного выполнения возвращает массив объектов, каждый из которых содержит стандартные поля:
    # tzid — идентификатор операции;
    # service — cервис в котором использовался номер;
    # phone — выделенный номер;
    # answer — полученный ответ (смс);
    # status — состояние операции: значения состояний такие же как возвращаемые методом getState

    def get_operations(opstate='active', count=100)
      method_query('getOperations', opstate: opstate, count: count)
    end

    # Отправляет уведомление об успешном получении кода и завершает операцию.
    def set_operation_ok(tzid)
      method_query('setOperationOk', tzid: tzid)
    end

    # Cоздает запрос на уточнение ответа по операции.
    # Следует использовать в случае если поступил ошибочный код и позволяет перепроверить его и выслать еще раз уточненный.
    def set_operation_revise(tzid)
      method_query('setOperationRevise', tzid: tzid)
    end

    # Завершает операцию с уведомлением о неверном коде.
    # Следует использовать в случае если было запрошено уточнение и код все равно не подходит.
    def set_operation_over(tzid)
      method_query('setOperationOver', tzid: tzid)
    end

    # Сообщает, что выданный номер уже использован или заблокирован в сервисе для которого запрашивалась активация.
    # Внимание! Данный запрос никак не влияет состояние операции, а только отправляет уведомление оператору и
    # только после проверки оператором операция может быть завершена.
    def set_operation_used(tzid)
      method_query('setOperationUsed', tzid: tzid)
    end

    # Cоздает операцию на повторное использование ранее использованного номера.
    # Повтор нельзя провести по операции, которая сама была повтором по еще одной предыдущей операции.
    # Если необходимо провести несколько повторов то в параметре tzid следует передавать номер первоначальной операции,
    # в которой использовался необходимый номер
    def get_num_repeat(tzid)
      # TODO: Implement this
    end

    # Cоздает операцию на повторное использование ранее использованного номера, который в текущий момент находится в режиме оффлайн.
    # Оффлайн повтор проводится в виде одиночного заказа и результаты выполнения доступны как через апи "работа с заказами"
    # так и через веб-интерфейс сайта в разделе "оставить заказ". Время выполнения оффлайн повтора может быть до 48 часов.

    # Параметры:
    # tzid Идентификатор операции по которой требуется повтор. int (числовое значение), обязательный параметр.
    # type Тип действия: 0 - прислать текст смс; 1 - восстановить доступ к аккаунту.
    #   Если type=1, тогда необходимы следующие параметры:
    #     log Логин от аккаунта, который восстанавливается.
    #     pas Пароль от аккаунта, который восстанавливается.

    def get_num_repeat_offline(tzid)
      # TODO: Implement this
    end

    private

    def method_query(method, parameters = {})
      url = SERVICE_URL_MASK.sub('METHOD_NAME', method)
      parameters[:apikey] = API_KEY
      query(url, parameters)
    end

    def query(url, params)
      uri = URI(url)

      uri.query = URI.encode_www_form(params)

      parse_response(Net::HTTP.get_response(uri))
    end

    def parse_response(resp)
      raise SMSReg::FailedRequest, "Server responded: #{resp.code} - #{resp.msg}" unless resp.code.to_i == 200
      result = JSON.parse(resp.body)
      raise SMSReg::WrongRequest, "Request error: #{result['error_msg']}" if result.is_a?(Hash) && result['response'] && result['response'] == 'ERROR'
      result
    end

  end
end