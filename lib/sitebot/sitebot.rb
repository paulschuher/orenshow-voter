# Require website drivers
puts Dir["./lib/sitebot/sites/*.rb"].each {|file| require file }

module Sitebot extend self
  def create(site)
    Object.const_get("Sitebot::Sites::#{site.capitalize}").new
  end

  class ExpectationNotEnsured < RuntimeError

  end
end