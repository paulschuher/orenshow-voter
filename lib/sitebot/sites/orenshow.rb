module Sitebot
module Sites

require "selenium-webdriver"

class Orenshow
  BAND_ID = 113496

  def begin_session
    @browser = Selenium::WebDriver.for :firefox
    @browser.navigate.to "http://orenshow.ru"
    sleep 10
    @browser.execute_script("show_vote(#{BAND_ID});")
    sleep 5
  end

  def type_number(number)
    input = @browser.find_element css: ".vote_form_input[name='phone']"
    input.send_keys number
    sleep 2
    @browser.find_element(css: "input[name='submit_get_sms']").click
    sleep 2
  end

  def type_code(code)
    input = @browser.find_element css: ".vote_form_input[name='code']"
    input.send_keys code
    sleep 2
    @browser.find_element(css: "input[name='submit_vote']").click
    sleep 2
  end

  def close_session
    @browser.quit
  end

  def wait_for
    wait = Selenium::WebDriver::Wait.new(:timeout => 1) # seconds
    wait.until {  }
  end

  def ensure_vote_ready
    ensure_element { @browser.find_element(css: "#video_id[value='#{BAND_ID}']") }
  end

  def ensure_number_typed(number)

  end


  private

  def ensure_element
    raise Sitebot::ExpectationNotEnsured unless yield
  end
end
end
end